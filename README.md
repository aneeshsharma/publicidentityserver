# PublicIdentityServer

A simple UDP server that returns the public IP and port number of the client and
also acts as a tracker server.

## Format

The server can be sent 2 different commands
- `=<id>` - To update the public location of `<id>`
- `?<id>` - To get the public location of `id`

The total query length is 16 bytes, thus the `<id>` can be maximum 15 bytes
(trailing whitespaces and newline characters are not considered).

If the command is not in the above 2 formats, the server simply returns the public
location of the client.

The format of reply is

```
<public IP>
<port number>
```
> Public IP and port number divided by a `\n` character

For example,
```
1.2.3.4
5678
```

