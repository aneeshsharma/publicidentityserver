import socket
from config import PORT

def encodeReply(addr):
    reply = "\n".join([str(e) for e in addr])
    return reply.encode('utf-8')

peers = {}

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.bind(("0.0.0.0", PORT))

print(f'Listening on {PORT}')

while True:
    data_raw, addr = sock.recvfrom(16)

    data = data_raw.decode('utf-8')

    result = encodeReply(addr)

    if data[0] == '=':
        identity = data[1:].strip()
        peers[identity] = addr
    elif data[0] == '?':
        identity = data[1:].strip()
        try:
            result = encodeReply(peers[identity])
        except KeyError:
            result = b'none'

    sock.sendto(result, addr)
